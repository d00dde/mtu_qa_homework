module.exports = {
  extends: "mtu-school",
  parserOptions: {
    project: "./tsconfig.json",
    tsconfigRootDir: __dirname,
  },
  rules: {
    "linebreak-style": ["error", "windows"],
  },
};
