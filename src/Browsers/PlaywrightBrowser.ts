import * as Playwright from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import { BrowserType } from "../types/BrowserTypes";
import { SignUpRequest } from "../types/SignUpRequest";
import { Browser } from "../types/Browser";

export class PlaywrightBrowser implements Browser {
  private readonly launchOption: Playwright.LaunchOptions = {
    headless: false,
    devtools: false,
  };

  private browser!: Playwright.Browser;

  private page!: Playwright.Page;

  private route: any;

  constructor(private type: BrowserType) {
  }

  async initPage(url: string): Promise<void> {
    this.browser = await Playwright[this.type].launch(this.launchOption);
    this.page = await this.browser.newPage();
    await this.page.goto(url);
  }

  createElement(selector: string): WebElement {
    return new WebElement(this.page, selector);
  }

  close(): void {
    this.browser.close();
  }

  interceptRequest(url: string, redirectUrl: string, calback: (request: SignUpRequest) => Promise<void>): void {
    this.page.route(url, (route, request) => {
      this.route = route;
      calback({
        method: request.method(),
        postData: request.postData() || "",
      });
      this.route.fulfill({
        status: 302,
        headers: {
          Location: redirectUrl,
        },
      });
    });
  }

  async pause(ms: number): Promise<void> {
    await this.page.waitForTimeout(ms);
  }

  waitForData<K extends object>(obj: K, fieldName: keyof K, timeout?: number): Promise<void>{
    return Promise.resolve();
  }
}
