import { WebElement } from "../utils/WDIOWebElement";
import { BrowserType } from "../types/BrowserTypes";
import { SignUpRequest } from "../types/SignUpRequest";
import { Browser } from "../types/Browser";

export class WebdriwerIOBrowser implements Browser {

  private page: any;

  constructor(private type: BrowserType) {
  }

  async initPage(url: string): Promise<void> {
    await browser.url(url);
    const puppeteer = await browser.getPuppeteer();
    browser.call(async () => {
      this.page = (await puppeteer.pages())[0];
      await this.page.setRequestInterception(true);
    });
  }

  createElement(selector: string): WebElement {
    return new WebElement(selector);
  }

  close(): void {}

  async interceptRequest(url: string, redirectUrl: string, calback: (request: SignUpRequest) => Promise<void>): Promise<void> {
    this.page.on('request', (interceptedRequest: any) => {
      if (interceptedRequest.url() === url) {
          calback({
            method: interceptedRequest.method(),
            postData: interceptedRequest.postData() || "",
          })
          interceptedRequest.continue({ url: redirectUrl });
      }
      interceptedRequest.continue();
    });
  }

  async pause(ms: number): Promise<void> {
    await browser.pause(ms);
  }

  waitForData<K extends object>(obj: K, fieldName: keyof K, timeout: number = 5000): Promise<void>{
    return browser.waitUntil(() => !!obj[fieldName], {
      timeout, 
      interval: 200,
      timeoutMsg: `No set ${fieldName} after ${timeout} milliseconds`,
    });
  }
}
