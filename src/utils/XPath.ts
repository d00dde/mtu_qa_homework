export class XPath {
  public readonly formPath = "//form[contains(@class, 'signup-form')]";

  public readonly signUpBtnPath = "//div[@data-slideto = 'signup'][contains(@class, 'button')]";

  public readonly popupPath = "//div[@class = 'select__dropdown']";

  public readonly selectElementPath = "/div[contains(@class, 'select')]";

  getInputPath(name: string): string {
    return `//input[@name = '${name}']`;
  }

  getButtonPath(className: string): string {
    return `//*[contains(@class, '${className}')]`;
  }

  getSelectorRootPath(name: string): string {
    return `//*[@name = '${name}']/parent::*`;
  }

  getOptionPath(className: string, option: string): string {
    return `//div[contains(@class, '${className}')][text() = "${option}"]`;
  }

  getCheckboxPath(name: string): string {
    return `//input[@name='${name}']/parent::*//label`;
  }
}
