import { CLI_EPILOGUE } from "@wdio/cli/build/constants";

export class WebElement {
  constructor(private selector: string) {}

  get selectorValue() {
    return this.selector;
  }

  getChildElement(selector: string): WebElement {
    return new WebElement(`${this.selectorValue}${selector}`);
  }

  async click(): Promise<void> {
    const element = await $(this.selectorValue);
    await element.waitForClickable();
    await element.click();
  }
  
    async type(text: string): Promise<void> {
      const element = await $(this.selectorValue);
      await element.waitForClickable();
      await element.setValue(text);
    }

  async select(...option: Array<string>): Promise<Array<string>> {
    const element = $(this.selectorValue);
    await element.waitForClickable();
    return element.selectOption(option);
  }

  async waitForElement(ms: number = 4000): Promise<void> {
    const element = await $(this.selectorValue);
    element.waitForClickable({ timeout: ms });
  }
}
