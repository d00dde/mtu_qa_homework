import { WebElement } from "../types/WebElement";
import { Browser } from "../types/Browser";
import { SignUpData } from "../types/signUp";
import { ElementsPathes } from "../types/ElementsPathes";
import Select from "../component/Select";

export default class StepOne {
  private form: WebElement;

  private firstNameInput: WebElement;

  private lastNameInput: WebElement;

  private emailInput: WebElement;

  private countrySelect: Select;

  private passInput: WebElement;

  private confirmPassInput: WebElement;

  private companyNameInput: WebElement;

  private companyTypeSelect: Select;

  private nextButton: WebElement;

  constructor(public browser: Browser, pathes: ElementsPathes) {
    this.form = browser.createElement(pathes.formPath);
    this.firstNameInput = this.form.getChildElement(pathes.getInputPath("firstName"));
    this.lastNameInput = this.form.getChildElement(pathes.getInputPath("lastName"));
    this.emailInput = this.form.getChildElement(pathes.getInputPath("email"));
    this.countrySelect = new Select(browser, pathes, "country");
    this.passInput = this.form.getChildElement(pathes.getInputPath("password"));
    this.confirmPassInput = this.form.getChildElement(pathes.getInputPath("confirmation"));
    this.companyNameInput = this.form.getChildElement(pathes.getInputPath("companyName"));
    this.companyTypeSelect = new Select(browser, pathes, "companyType");
    this.nextButton = this.form.getChildElement(pathes.getButtonPath("signup-form__next"));
  }

  async fillForm({
    firstName,
    lastName,
    email,
    country,
    password,
    confirmation,
    companyName,
    companyType,
  }: SignUpData): Promise<void> {
    await this.firstNameInput.type(firstName);
    await this.lastNameInput.type(lastName);
    await this.emailInput.type(email);
    await this.countrySelect.select(country);
    await this.passInput.type(password);
    await this.confirmPassInput.type(confirmation);
    await this.companyNameInput.type(companyName);
    await this.companyTypeSelect.select(companyType, "select__option-title");
    // await this.browser.pause(2000);
    return this.nextButton.click();
  }
}
