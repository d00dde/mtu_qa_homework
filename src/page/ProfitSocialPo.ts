import { WebElement } from "../types/WebElement";
import { Browser } from "../types/Browser";
import { SignUpData } from "../types/signUp";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import { XPath } from "../utils/XPath";
import { ElementsPathes } from "../types/ElementsPathes";

export class ProfitSocialPo {
  private signUpButton: WebElement;

  private stepOne: StepOne;

  private stepTwo: StepTwo;

  private pathes: ElementsPathes;

  constructor(public browser: Browser) {
    this.pathes = new XPath();
    this.signUpButton = browser.createElement(this.pathes.signUpBtnPath);
    this.stepOne = new StepOne(browser, this.pathes);
    this.stepTwo = new StepTwo(browser, this.pathes);
  }

  async signUp(data: SignUpData): Promise<string> {
    await this.signUpButton.click();
    await this.stepOne.fillForm(data);
    return this.stepTwo.signUp(data);
  }

  static async initPage(browser: Browser): Promise<ProfitSocialPo> {
    await browser.initPage("https://www.profitsocial.com/en");
    return new ProfitSocialPo(browser);
  }
}
