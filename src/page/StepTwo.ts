import { WebElement } from "../types/WebElement";
import { Browser } from "../types/Browser";
import { SignUpData } from "../types/signUp";
import { SignUpRequest } from "../types/SignUpRequest";
import { ElementsPathes } from "../types/ElementsPathes";
import Select from "../component/Select";
import Checkbox from "../component/Checkbox";

export default class StepTwo {
  private readonly registerUrl = "https://profitsocial.com/api/profitsocialReg";

  private readonly redirectUrl = "https://profitsocial.com/api/profitsocialReg";

  private form: WebElement;

  private messengerSelect: Select;

  private messengerNikNameInput: WebElement;

  private trafficTypeSelect: Select;

  private revenueSelect: Select;

  private topCountriesInput: WebElement;

  private howFoundOutSelect: Select;

  private termsCheckbox: Checkbox;

  private policyCheckbox: Checkbox;

  private signUpButton: WebElement;

  private postData: Promise<string>;

  private isPOSTSignUp: boolean;

  constructor(public browser: Browser, pathes: ElementsPathes) {
    this.browser = browser;
    this.postData = Promise.resolve("");
    this.isPOSTSignUp = false;
    this.form = browser.createElement(pathes.formPath);
    this.messengerSelect = new Select(browser, pathes, "imType");
    this.messengerNikNameInput = this.form.getChildElement(pathes.getInputPath("imName"));
    this.trafficTypeSelect = new Select(browser, pathes, "trafficType");
    this.revenueSelect = new Select(browser, pathes, "revenue");
    this.topCountriesInput = this.form.getChildElement(pathes.getInputPath("topCountries"));
    this.howFoundOutSelect = new Select(browser, pathes, "howFoundOut");
    this.termsCheckbox = new Checkbox(this.form, pathes, "terms");
    this.policyCheckbox = new Checkbox(this.form, pathes, "policy");
    this.signUpButton = this.form.getChildElement(pathes.getButtonPath("signup-form__submit"));
  }

  async signUp({
    imType,
    imName,
    trafficType,
    revenue,
    topCountries,
    howFoundOut,
    terms,
    policy,
  }: SignUpData): Promise<string> {
    await this.messengerSelect.select(imType);
    await this.messengerNikNameInput.type(imName);
    await this.trafficTypeSelect.select(trafficType);
    await this.revenueSelect.select(revenue);
    await this.topCountriesInput.type(topCountries);
    await this.howFoundOutSelect.select(howFoundOut, "select__option-title");
    await this.termsCheckbox.check(terms);
    await this.policyCheckbox.check(policy);
    await this.interceptSignUp();
    await this.signUpButton.click();
    await this.browser.waitForData(this, "isPOSTSignUp");
    if (!this.isPOSTSignUp) {
      throw new Error(`The expected sign up request was not send to ${this.registerUrl}`);
    }
    return this.postData;
  }

  async interceptSignUp() {
    this.browser.interceptRequest(this.registerUrl, this.redirectUrl, async (request: SignUpRequest) => {
      if (request.method === "POST") {
        this.isPOSTSignUp = true;
        this.postData = new Promise((resolve) => {
          resolve(request.postData);
        });
      }
    });
  }
}
