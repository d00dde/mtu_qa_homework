import {
  Country,
  CompanyType,
  HowFoundOut,
  Messenger,
  Revenue,
  TrafficType,
} from "./selector-enums";

export type SignUpData = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmation: string;
  companyName: string;
  country: Country;
  companyType: CompanyType;
  imType: Messenger;
  imName: string;
  trafficType: TrafficType;
  revenue: Revenue;
  topCountries: string;
  howFoundOut: HowFoundOut;
  terms: "0" | "1";
  policy: "0" | "1";
};
