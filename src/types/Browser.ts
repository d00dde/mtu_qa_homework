import { WebElement } from "./WebElement";
import { SignUpRequest } from "./SignUpRequest";

export type Browser = {
  initPage: (url: string) => Promise<void>,
  createElement: (selector: string) => WebElement,
  close: () => void,
  interceptRequest: (url: string, redirectUrl: string, calback: (request: SignUpRequest) => Promise<void>) => void;
  pause: (ms: number) => Promise<void>;
  waitForData: <K extends object>(obj: K, fieldName: keyof K, timeout: number) => Promise<void>;
};
