export type SelectorPathes = {
  popupPath: string;
  selectElementPath: string;
  getOptionPath(className: string, option: string): string;
  getSelectorRootPath(name: string): string;
};
export type CheckboxPathes = {
  getCheckboxPath(name: string): string;
};

export type ElementsPathes = SelectorPathes & CheckboxPathes &{
  formPath: string;
  signUpBtnPath: string;
  getInputPath(name: string): string;
  getButtonPath(className: string): string;
};
