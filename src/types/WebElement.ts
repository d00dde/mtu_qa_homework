export type WebElement = {
  click(): Promise<void>;
  getChildElement(selector: string): WebElement;
  select(...option: Array<string>): Promise<Array<string>>;
  type(text: string): Promise<void>;
  waitForElement(ms?: number): Promise<void>;
};
