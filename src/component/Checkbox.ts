import { WebElement } from "../types/WebElement";
import { CheckboxPathes } from "../types/ElementsPathes";

export default class Select {
  private checkbox: WebElement;

  constructor(
    protected form: WebElement,
    pathes: CheckboxPathes,
    selector: string
  ) {
    this.checkbox = form.getChildElement(pathes.getCheckboxPath(selector));
  }

  async check(expectedState: string): Promise<void> {
    if (expectedState === "1") {
      await this.checkbox.click();
    }
  }
}
