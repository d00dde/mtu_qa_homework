import { SelectorPathes } from "../types/ElementsPathes";
import { WebElement } from "../types/WebElement";
import { Browser } from "../types/Browser";

export default class Select {
  private root: WebElement;

  private selectElement: WebElement;

  private popup: WebElement;

  constructor(
    protected browser: Browser,
    private pathes: SelectorPathes,
    selector: string
  ) {
    this.root = browser.createElement(this.pathes.getSelectorRootPath(selector));
    this.selectElement = this.root.getChildElement(this.pathes.selectElementPath);
    this.popup = this.selectElement.getChildElement(this.pathes.popupPath);
  }

  async select(option: string, className: string = "select__option"): Promise<void> {
    await this.selectElement.click();
    await this.popup.waitForElement();
    const optionElement = this.getOptionByText(option, className);
    return optionElement.click();
  }

  getOptionByText(option: string, className: string): WebElement {
    return this.popup.getChildElement(this.pathes.getOptionPath(className, option));
  }
}
