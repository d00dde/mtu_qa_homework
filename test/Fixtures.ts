import * as faker from "faker";
import { SignUpData } from "../src/types/signUp";
import {
  Country,
  CompanyType,
  HowFoundOut,
  Messenger,
  Revenue,
  TrafficType,
} from "../src/types/selector-enums";

export class Fixtures {
  getValidData(): SignUpData {
    return {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      country: faker.random.arrayElement(Object.values(Country)),
      password: "JE5PisYeaR3H",
      confirmation: "JE5PisYeaR3H",
      companyName: faker.company.companyName(),
      companyType: CompanyType.AFFILIATE_NETWORK, // faker.random.arrayElement(Object.values(CompanyType)),
      imType: faker.random.arrayElement(Object.values(Messenger)),
      imName: faker.internet.userName(),
      trafficType: faker.random.arrayElement(Object.values(TrafficType)),
      revenue: faker.random.arrayElement(Object.values(Revenue)),
      topCountries: [
        faker.random.arrayElement(Object.values(Country)),
        faker.random.arrayElement(Object.values(Country)),
      ].join(", "),
      howFoundOut: faker.random.arrayElement(Object.values(HowFoundOut)),
      terms: "1",
      policy: "1",
    };
  }
}
