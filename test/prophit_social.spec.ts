import { assert } from "chai";
// import { PlaywrightBrowser } from "../src/Browsers/PlaywrightBrowser";
import { WebdriwerIOBrowser } from "../src/Browsers/WebdriwerIOBrowser";
import { Browser } from "../src/types/Browser";
import { BrowserType } from "../src/types/BrowserTypes";
import { ProfitSocialPo } from "../src/page/ProfitSocialPo";
import { Fixtures } from "./Fixtures";

let browser: Browser;
const fixtures = new Fixtures();

describe("Profit Social test", () => {
  afterEach(async () => {
    await browser.close();
  });

  it("Sign Up", async () => {
    browser = new WebdriwerIOBrowser(BrowserType.CHROMIUM);
    const PSpage = await ProfitSocialPo.initPage(browser);
    const formData = fixtures.getValidData();
    const postData = await PSpage.signUp(formData);
    assert.deepEqual(formData, JSON.parse(postData));
  });
});
